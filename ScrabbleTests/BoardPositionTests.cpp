#include "stdafx.h"
#include "CppUnitTest.h"
#include "BoardPosition.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(BoardPositionTests)
	{
	public:

		TEST_METHOD(ConstructorAndGetPositionTest)
		{
			BoardPosition position;
			Assert::IsTrue(position.GetPosition() == 0);
		}

	};
}