#include "stdafx.h"
#include "CppUnitTest.h"
#include "Player.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(PlayerTests)
	{
	public:

		TEST_METHOD(ConstructorTest)
		{
			Player testPlayer;
			Assert::IsTrue(testPlayer.GetScore() == 0 && testPlayer.GetName() == "notSet");
		}
		TEST_METHOD(IncrementScoreTest)
		{
			Player testPlayer;
			testPlayer.incrementScore(500);
			Assert::IsTrue(testPlayer.GetScore() == 500);
		}
		TEST_METHOD(TakePieceTest)
		{
			Player testPlayer;
			Piece piece(Piece::Letter::C);
			testPlayer.takePiece(piece);
			Assert::IsTrue(testPlayer.seePiece(NULL).toChar()==piece.toChar());
		}
		TEST_METHOD(PutPieceTest)
		{
			Player testPlayer;
			Piece piece(Piece::Letter::A);
			testPlayer.takePiece(piece);
			testPlayer.putPiece(0);
			Assert::IsTrue(testPlayer.GetNumberOfPieces()==0);
		}

	};
}