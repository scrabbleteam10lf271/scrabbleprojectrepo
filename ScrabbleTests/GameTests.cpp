#include "stdafx.h"
#include "CppUnitTest.h"
#include "Game.h"

#include <vector>
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(GameTests)
	{
	public:

		TEST_METHOD(PositionValidationInvalid)
		{
			Game gameTest;
			std::vector<std::pair<Piece, int>>testMove;
			testMove.emplace_back(Piece(Piece::Letter::A), 12);
			testMove.emplace_back(Piece(Piece::Letter::B), 90);
			Assert::IsTrue(gameTest.validatePositioning(testMove)==Game::MoveType::Invalid);
		}
		TEST_METHOD(PositionValidationVertical)
		{
			Game gameTest;
			std::vector<std::pair<Piece, int>>testMove;
			testMove.emplace_back(Piece(Piece::Letter::M), 12);
			testMove.emplace_back(Piece(Piece::Letter::Z), 27);
			Assert::IsTrue(gameTest.validatePositioning(testMove) == Game::MoveType::Vertical);
		}
		TEST_METHOD(PositionValidationOrizontal)
		{
			Game gameTest;
			std::vector<std::pair<Piece, int>>testMove;
			testMove.emplace_back(Piece(Piece::Letter::M), 12);
			testMove.emplace_back(Piece(Piece::Letter::Z), 13);
			Assert::IsTrue(gameTest.validatePositioning(testMove) == Game::MoveType::Orizontal);
		}
		TEST_METHOD(MakeWordTest)
		{
			Game gameTest;
			std::vector<std::pair<Piece, int>>testMove;
			testMove.emplace_back(Piece(Piece::Letter::C), 12);
			testMove.emplace_back(Piece(Piece::Letter::A), 13);
			testMove.emplace_back(Piece(Piece::Letter::N), 14);
			std::string word=gameTest.makeWord(testMove, 1);
			Assert::IsTrue(word == "CAN");
		}
		TEST_METHOD(CheckWordTest)
		{
			Game gameTest;
			std::vector<std::pair<Piece, int>>testMove;
			testMove.emplace_back(Piece(Piece::Letter::M), 111);
			testMove.emplace_back(Piece(Piece::Letter::A), 112);
			testMove.emplace_back(Piece(Piece::Letter::K), 113);
			testMove.emplace_back(Piece(Piece::Letter::E), 114);
			Assert::IsTrue(gameTest.checkIfWordIsReal(gameTest.makeWord(testMove, 1)));
		}
		TEST_METHOD(FindWinnerTest)
		{
			Game gameTest;
			std::vector<Player>testSubjects;
			testSubjects.emplace_back();
			testSubjects.emplace_back();
			testSubjects[0].incrementScore(100);
			testSubjects[1].incrementScore(123);
			Assert::IsTrue(gameTest.GetWinner(testSubjects)->GetScore() == 123);
		}
		



	};
}