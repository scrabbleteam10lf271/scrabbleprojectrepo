#include "stdafx.h"
#include "CppUnitTest.h"
#include "Dictionary.h"

#include <fstream>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(DictionaryTests)
	{
	public:

		TEST_METHOD(ConstructorTest)
		{
			
			Dictionary dictionary(std::ifstream("Dictionary.txt"));
			std::string testWord = "BUTTERFLY";
			Assert::IsFalse(dictionary.isAWord(testWord));
		}

		TEST_METHOD(ConstructorTestWithMissingWord)
		{
			Dictionary dictionary(std::ifstream("Dictionary.txt"));
			std::string testWord = "MASINA";
			Assert::IsFalse(dictionary.isAWord(testWord));
		}

	};
}