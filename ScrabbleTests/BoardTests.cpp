#include "stdafx.h"
#include "CppUnitTest.h"
#include "Board.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(BoardTests)
	{
	public:

		TEST_METHOD(ConstructorTestMethod)
		{
			Board board;
			Assert::IsTrue(board[Board::BoardSize *7 +7].first == Board::TileType::ST);
			
		}

	

		TEST_METHOD(TestForPutPieceMethod)
		{
			Board board;
			Piece  piece(Piece::Letter::A);
			board.putPiece(piece,NULL);
			Assert::IsTrue(board[NULL].second.has_value());
			
		}



	};
}