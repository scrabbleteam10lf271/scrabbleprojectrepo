#include "stdafx.h"
#include "CppUnitTest.h"
#include "Bag.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(BagTests)
	{
	public:

		TEST_METHOD(ConstructorTest)
		{
			Bag bag;
			Assert::IsFalse(bag.isEmpty());
		}

		TEST_METHOD(TakeOnePieceMethodTest1)
		{
			Bag bag;
			Piece piece = bag.takeOnePiece();
			Assert::IsTrue(piece.toChar() !='A');
		}

		TEST_METHOD(TakeOnePieceMethodTest2)
		{
			Bag bag;
			Piece piece = bag.takeOnePiece();
			Assert::IsFalse(piece.toChar() == ' ');
		}

	};
}