#include "stdafx.h"
#include "CppUnitTest.h"
#include "Piece.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ScrabbleTests
{
	TEST_CLASS(PieceTests)
	{
	public:

		TEST_METHOD(VerifyingValueOfPiece)
		{
			Piece piece(Piece::Letter::A);
			Assert::IsTrue(piece.getValue() == 1);
		}

		TEST_METHOD(TestMethodConstructorWithParameters)
			{
			Piece piece(Piece::Letter::A);
			Assert::IsTrue(Piece::Letter::A == piece.getLetter());
			Assert::IsTrue(piece.getValue() == 1);
			}
		
			TEST_METHOD(TestForToCharMethod)
			 {
			Piece piece(Piece::Letter::D);
			Assert::IsTrue(piece.toChar() == 'D');
			
			}

	};
}