#include "Piece.h"
#include <iostream>


Piece::Piece()
{
}


Piece::~Piece()
{
}

Piece::Piece(const Piece::Letter & letter)
{
	this->m_letter = letter;
	switch (letter)
	{
	case Piece::Letter::A:
		this->m_value = 1;
		break;
	case Piece::Letter::B:
		this->m_value = 3;
		break;
	case Piece::Letter::C:
		this->m_value = 3;
		break;
	case Piece::Letter::D:
		this->m_value = 2;
		break;
	case Piece::Letter::E:
		this->m_value = 1;
		break;
	case Piece::Letter::F:
		this->m_value = 4;
		break;
	case Piece::Letter::G:
		this->m_value = 2;
		break;
	case Piece::Letter::H:
		this->m_value = 4;
		break;
	case Piece::Letter::I:
		this->m_value = 1;
		break;
	case Piece::Letter::J:
		this->m_value = 8;
		break;
	case Piece::Letter::K:
		this->m_value = 5;
		break;
	case Piece::Letter::L:
		this->m_value = 1;
		break;
	case Piece::Letter::M:
		this->m_value = 3;
		break;
	case Piece::Letter::N:
		this->m_value = 1;
		break;
	case Piece::Letter::O:
		this->m_value = 1;
		break;
	case Piece::Letter::P:
		this->m_value = 3;
		break;
	case Piece::Letter::Q:
		this->m_value = 10;
		break;
	case Piece::Letter::R:
		this->m_value = 1;
		break;
	case Piece::Letter::S:
		this->m_value = 1;
		break;
	case Piece::Letter::T:
		this->m_value = 1;
		break;
	case Piece::Letter::U:
		this->m_value = 1;
		break;
	case Piece::Letter::V:
		this->m_value = 4;
		break;
	case Piece::Letter::W:
		this->m_value = 4;
		break;
	case Piece::Letter::X:
		this->m_value = 8;
		break;
	case Piece::Letter::Y:
		this->m_value = 4;
		break;
	case Piece::Letter::Z:
		this->m_value = 10;
		break;
	case Piece::Letter::None:
		this->m_value = 0;
		break;

	}
}

int Piece::getValue() const
{
	return m_value;
}

void Piece::setValue(int value)
{
	this->m_value = value;
}

Piece::Letter Piece::getLetter() const
{
	return m_letter;
}



void Piece::setLetter(const Letter & value)
{
	this->m_letter = value;
}


char Piece::toChar() const
{
	switch (m_letter)
	{
	case Piece::Letter::A:
		return 'A';
	case Piece::Letter::B:
		return'B';
	case Piece::Letter::C:
		return'C';
	case Piece::Letter::D:
		return'D';
	case Piece::Letter::E:
		return'E';
	case Piece::Letter::F:
		return'F';
	case Piece::Letter::G:
		return'G';
	case Piece::Letter::H:
		return'H';
		
	case Piece::Letter::I:
		return'I';
		
	case Piece::Letter::J:
		return'J';
		
	case Piece::Letter::K:
		return'K';

	case Piece::Letter::L:
		return'L';
	
	case Piece::Letter::M:
		return'M';
	
	case Piece::Letter::N:
		return'N';
		
	case Piece::Letter::O:
		return'O';
		
	case Piece::Letter::P:
		return'P';
		
	case Piece::Letter::Q:
		return'Q';
		
	case Piece::Letter::R:
		return'R';
	
	case Piece::Letter::S:
		return'S';
	
	case Piece::Letter::T:
		return'T';
		
	case Piece::Letter::U:
		return'U';
	
	case Piece::Letter::V:
		return'V';
	
	case Piece::Letter::W:
		return'W';
		
	case Piece::Letter::X:
		return'X';
	
	case Piece::Letter::Y:
		return'Y';

	case Piece::Letter::Z:
		return'Z';
	case Piece::Letter::None:
		return' ';
		break;

	}
}




std::ostream & operator <<(std::ostream & os, const Piece &piece) {
	
	os << piece.toChar();

	return os;
}