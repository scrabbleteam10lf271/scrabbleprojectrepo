#pragma once

#include <vector>
#include <winsock2.h>

#include "Player.h"
#include "Board.h"
#include "Bag.h"
#include "Dictionary.h"
#include "Piece.h"
#include "BoardPosition.h"


class Game
{
private:
	Bag m_playingBag;
	Board m_playingBoard;
	Dictionary m_dictionary;
public:
	enum class TurnStates
	{
		Choose, PutPieces, ExchangePieces, NextPlayer
	};
	enum class MoveType
	{
		Invalid, Vertical, Orizontal
	};
	static const unsigned char numberOfPlayerPieces = 7;
	MoveType validatePositioning(const std::vector<std::pair< Piece, int>>&playerMove);
	bool validatePlayerMove(const std::vector<std::pair< Piece, int>>&playerMove);
	bool moveCanBeDone(const std::vector<std::pair< Piece, int>>&playerMove, int increment);
	std::string makeWord(const std::vector<std::pair< Piece, int>>&playerMove, int increment);
	bool checkIfWordIsReal(std::string word);
	void makeMove(const std::vector<std::pair< Piece, int>>&playerMove);
	int valueOfNewWord(int firstLetter, int lastLetter, int increment);
	void findNewWordBounds(int& firstLetter, int& lastLetter, int increment);
	std::vector<Player>::iterator GetWinner(std::vector<Player>players);
	std::vector<std::pair<Player, SOCKET>>::iterator GetWinner(std::vector<std::pair<Player, SOCKET>>players);
	std::string cleanReceivedBuffer(const char *buffer);
public:
	void play();
	void lanPlay();
	Game();
};

