#pragma once
#include "Piece.h"


#include <string>
#include <vector>
#include <iostream>
class Player
{
public:
	friend std::istream& operator>>(std::istream& stream,Player& object);
	friend std::ostream& operator<<(std::ostream& stream, Player& object);
	int GetScore();
	void incrementScore(int with);
	void setName(const std::string name);
	void takePiece(const Piece & piece);
	const Piece& seePiece(int index);
	Piece&& putPiece(int index);
	const std::string& GetName();
	int GetNumberOfPieces();
	const std::string& getPlayerBoardString();

public:
	Player();
	~Player();
private:
	std::string m_name;
	int m_score;
	std::vector<Piece>m_playerPieces;


};

