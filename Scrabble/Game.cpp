#undef UNICODE
#define WIN32_LEAN_AND_MEAN

#define DEFAULT_PORT "27015"
#define DEFAULT_BUFLEN 10240

#include "Game.h"
#include "Logging/Logging.h"

#include <stack>
#include <unordered_set>
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <sstream>

#pragma comment (lib, "Ws2_32.lib")


bool compare(std::pair< Piece, int> first, std::pair< Piece, int>second)
{
	if (first.second < second.second)
		return true;
	return false;
}



void Game::play()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger gameLogger(of);
	gameLogger.log("Game Started ...", Logger::Level::Info);
	std::cout << m_playingBoard;
	Board::GameLegend();
	BoardPosition pos;
	int playerCount, playerChoice = -1;
	int tempIndex, centerOfBoard = 7 * 15 + 7;
	char smallestLetter = 'Z' + 1;
	Piece auxPiece;
	std::vector<Player> players;
	std::vector<Player>::iterator playerIterator;
	std::cout << "Type player count(2-4) :";
	std::cin >> playerCount;
	std::cout << "\n";
	switch (playerCount)
	{
	default:
		gameLogger.log("Invalid player count input!\nExiting game....", Logger::Level::Error);
		std::cout << "Invalid player count!\nGAME OVER!\nExiting.....\n";
		exit(0);
	case 2:
	case 3:
	case 4:
		break;
	}

	for (auto index = 0; index < playerCount; index++)
	{
		players.emplace_back();
		std::cout << "\nPlayer : " << index + 1 << "\nEnter your name : ";
		std::cin >> players[index];
	}
	std::cout << "Each player needs to draw his first piece\n";
	for (auto playerIt = players.begin(); playerIt < players.end(); playerIt++)
	{
		Piece auxPiece = m_playingBag.takeOnePiece();
		char auxLetter = auxPiece.toChar();
		m_playingBag.putPieceBackInBag(auxPiece);
		if (auxLetter < smallestLetter)
		{
			smallestLetter = auxLetter;
			playerIterator = playerIt;
		}
		std::cout << "Player " << playerIt->GetName() << " drew letter " << auxLetter << "\n";
	}
	std::cout << "Starting player is " << (*playerIterator).GetName() << "\n";

	for (auto playerIt = players.begin(); playerIt < players.end(); playerIt++)
	{
		for (unsigned char index = 0; index < numberOfPlayerPieces; index++)
		{
			Piece takenPiece = m_playingBag.takeOnePiece();
			playerIt->takePiece(takenPiece);
		}
	}

	try
	{
		while (!m_playingBag.isEmpty())
		{
			std::cout << m_playingBoard;
			std::unordered_set<int>checkPiecePos, checkBoardPos;
			std::set<int> pieceIndexes;
			int newWordStart, newWordEnd;
			std::vector<int>pieceIndexesInMove;
			std::vector<std::pair< Piece, int>>playerMove;
			std::cout << "\n" << playerIterator->GetName() << "'s turn:\n";
			std::cout << (*playerIterator);
			std::stack<Game::TurnStates>turnStates;
			Game::TurnStates currentState;
			turnStates.push(Game::TurnStates::Choose);
			Game::MoveType moveWay;
			while (!turnStates.empty())
			{
				currentState = turnStates.top();
				turnStates.pop();
				switch (currentState)
				{
				case Game::TurnStates::Choose:
					std::cout << "\nYou can:\n1)Put pieces on the table\n2)Exchange pieces with new ones\n3)Pass\nWhat will you do: ";
					std::cin >> playerChoice;
					switch (playerChoice)
					{
					case 1:
						turnStates.push(Game::TurnStates::PutPieces);
						break;
					case 2:
						turnStates.push(Game::TurnStates::ExchangePieces);
						break;
					case 3:
						turnStates.push(Game::TurnStates::NextPlayer);
						break;
					default:
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input..", Logger::Level::Warning);
						std::cout << "\nWrong choice!!!";
						break;
					}
					break;
				case Game::TurnStates::PutPieces:
					std::cout << "\nHow many pieces do you want to put on the table ?(1-7) :";
					std::cin >> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(Game::TurnStates::Choose);
						std::cout << "\nWrong number of pieces!!!";


						break;
					}
					for (int index = 0; index < playerChoice; index++)
					{
						std::cout << "\nInsert where you want to put the piece (x,y) : ";
						std::cin >> pos;
						std::cout << "\nInsert piece index(1-7) : ";
						std::cin >> tempIndex;
						if (tempIndex < 1 || tempIndex>7 || pos.GetPosition() < 0 || pos.GetPosition() > Board::NumberOfTiles)
						{
							break;
						}
						tempIndex--;
						checkPiecePos.insert(tempIndex);
						checkBoardPos.insert(pos.GetPosition());
					}


					if (checkBoardPos.size() == checkPiecePos.size() && checkPiecePos.size() == playerChoice)
					{
						auto boardPosIt = checkBoardPos.begin();
						auto piecePosIt = checkPiecePos.begin();
						while (boardPosIt != checkBoardPos.end() && piecePosIt != checkPiecePos.end())
						{
							pieceIndexesInMove.push_back(*piecePosIt);
							playerMove.emplace_back(playerIterator->seePiece(*piecePosIt), *boardPosIt);
							boardPosIt++;
							piecePosIt++;
						}
					}
					else
					{
						std::cout << "\nInvalid move!!!";
						gameLogger.log("A player made wrong move ....", Logger::Level::Warning);
						turnStates.push(Game::TurnStates::Choose);
						break;
					}


					if (validatePlayerMove(playerMove) == true)
					{
						moveWay = validatePositioning(playerMove);
						std::sort(pieceIndexesInMove.rbegin(), pieceIndexesInMove.rend());
						std::sort(playerMove.begin(), playerMove.end(), compare);
						for (auto pos : pieceIndexesInMove)
						{
							playerIterator->putPiece(pos);
						}
						makeMove(playerMove);
						switch (moveWay)
						{
						case Game::MoveType::Vertical:
							newWordEnd = (playerMove.end() - 1)->second;
							newWordStart = playerMove.begin()->second;
							findNewWordBounds(newWordStart, newWordEnd, Board::BoardSize);
							playerIterator->incrementScore(valueOfNewWord(newWordStart, newWordEnd, Board::BoardSize));
							break;
						case Game::MoveType::Orizontal:
							newWordEnd = (playerMove.end() - 1)->second;
							newWordStart = playerMove.begin()->second;
							findNewWordBounds(newWordStart, newWordEnd, 1);
							playerIterator->incrementScore(valueOfNewWord(newWordStart, newWordEnd, 1));
							break;
						}
						turnStates.push(Game::TurnStates::NextPlayer);
					}
					else
					{
						gameLogger.log("A player made wrong move ....", Logger::Level::Warning);
						std::cout << "\nWrong move!!!";
						turnStates.push(Game::TurnStates::Choose);
					}
					break;
				case Game::TurnStates::ExchangePieces:
					std::cout << "\nHow many pieces do you want to exchange ?(1-7) : ";
					std::cin >> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input ....", Logger::Level::Warning);
						std::cout << "\nWrong number of pieces!!!";
						break;
					}
					for (int index = 0; index < playerChoice; index++)
					{
						std::cout << "\nInsert piece index(1-7) : ";
						std::cin >> tempIndex;
						if (tempIndex < 1 || tempIndex>7)
						{
							break;
						}
						tempIndex--;
						pieceIndexes.insert(tempIndex);
					}
					if (pieceIndexes.size() != playerChoice)
					{
						std::cout << "\nWrong piece index!!!";
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input ....", Logger::Level::Warning);
					}
					else
					{
						for (auto it = pieceIndexes.rbegin(); it != pieceIndexes.rend(); it++)
						{
							m_playingBag.putPieceBackInBag(playerIterator->putPiece(*it));
						}
						turnStates.push(Game::TurnStates::NextPlayer);
					}
					break;
				case Game::TurnStates::NextPlayer:
					while (!m_playingBag.isEmpty() && playerIterator->GetNumberOfPieces() < numberOfPlayerPieces)
					{
						auto taken = m_playingBag.takeOnePiece();
						playerIterator->takePiece(taken);
					}
					playerIterator++;
					if (playerIterator == players.end())
					{
						playerIterator = players.begin();
					}
					break;
				}
			}
			pieceIndexesInMove.clear();
			playerMove.clear();
		}
	}
	catch (...)
	{
		gameLogger.log("Unexpected error....\nExiting...", Logger::Level::Error);
		of.close();
		exit(NULL);
	}
	auto winner = GetWinner(players);
	gameLogger.log("A player won\nExiting.....", Logger::Level::Info);
	std::cout << "\nGAME OVER!\nWinner is : " << winner->GetName();
	of.close();
}

void Game::lanPlay()
{

	//WINSOCK


	WSADATA wsaData;
	int iResult;

	SOCKET ListenSocket = INVALID_SOCKET;

	struct addrinfo *result = NULL;
	struct addrinfo hints;

	int iSendResult;
	char recvbuf[DEFAULT_BUFLEN];
	int recvbuflen = DEFAULT_BUFLEN;

	WSAStartup(MAKEWORD(2, 2), &wsaData);

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);


	ListenSocket = socket(result->ai_family, result->ai_socktype, result->ai_protocol);

	bind(ListenSocket, result->ai_addr, (int)result->ai_addrlen);

	freeaddrinfo(result);
	listen(ListenSocket, SOMAXCONN);

	//WINSOCK
	std::string sendBuffaux,receivedBuff,auxbuff="";
	std::ofstream of("syslog.log", std::ios::app);
	Logger gameLogger(of);
	gameLogger.log("Game Started ...", Logger::Level::Info);
	sendBuffaux += m_playingBoard.toString();
	sendBuffaux += Board::GameLegend();
	BoardPosition pos;
	int playerCount, playerChoice = -1;
	int tempIndex, centerOfBoard = 7 * 15 + 7;
	char smallestLetter = 'Z' + 1;
	Piece auxPiece;
	std::vector<std::pair<Player, SOCKET>> players;
	std::vector<std::pair<Player, SOCKET>>::iterator playerIterator;

	players.emplace_back();
	players[0].second = accept(ListenSocket, NULL, NULL);
	players[0].first.setName("first");
	players.emplace_back();
	players[1].second = accept(ListenSocket, NULL, NULL);
	players[1].first.setName("second");
	closesocket(ListenSocket);

	sendBuffaux += "Each player needs to draw his first piece\n";
	for (auto playerIt = players.begin(); playerIt < players.end(); playerIt++)
	{
		Piece auxPiece = m_playingBag.takeOnePiece();
		char auxLetter = auxPiece.toChar();
		m_playingBag.putPieceBackInBag(auxPiece);
		if (auxLetter < smallestLetter)
		{
			smallestLetter = auxLetter;
			playerIterator = playerIt;
		}
		sendBuffaux += "Player #" + std::to_string(playerIt - players.begin()+1) + " drew letter " + auxLetter + "\n";
	}
	sendBuffaux += "Player #" + std::to_string(playerIterator - players.begin()+1) + " has the first move!\n";

	for (auto playerIt = players.begin(); playerIt < players.end(); playerIt++)
	{
		for (unsigned char index = 0; index < numberOfPlayerPieces; index++)
		{
			Piece takenPiece = m_playingBag.takeOnePiece();
			playerIt->first.takePiece(takenPiece);
		}
	}

		while (!m_playingBag.isEmpty())
		{

			for (auto player:players)
			{
				if (playerIterator->first.GetName() == player.first.GetName())
				{
					auxbuff = sendBuffaux + player.first.getPlayerBoardString()+"\nYour Turn!\n`";
				}
				else
				{
					auxbuff = sendBuffaux + "\nWait for your turn\nwait`";
				}
				send(player.second, auxbuff.c_str(), DEFAULT_BUFLEN, 0);
			}


			recv(playerIterator->second, recvbuf, recvbuflen, 0);
			receivedBuff = cleanReceivedBuffer(recvbuf);
			std::stringstream clientInput(receivedBuff);
			std::unordered_set<int>checkPiecePos, checkBoardPos;
			std::set<int> pieceIndexes;
			int newWordStart, newWordEnd;
			std::vector<int>pieceIndexesInMove;
			std::vector<std::pair< Piece, int>>playerMove;
			std::stack<Game::TurnStates>turnStates;
			Game::TurnStates currentState;
			turnStates.push(Game::TurnStates::Choose);
			Game::MoveType moveWay;
			while (!turnStates.empty())
			{
				currentState = turnStates.top();
				turnStates.pop();
				switch (currentState)
				{
				case Game::TurnStates::Choose:
					clientInput >> playerChoice;
					switch (playerChoice)
					{
					case 1:
						turnStates.push(Game::TurnStates::PutPieces);
						break;
					case 2:
						turnStates.push(Game::TurnStates::ExchangePieces);
						break;
					case 3:
						turnStates.push(Game::TurnStates::NextPlayer);
						break;
					default:
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input..", Logger::Level::Warning);
						send(playerIterator->second, "\nWrong choice!!!\n`", DEFAULT_BUFLEN, 0);
						recv(playerIterator->second, recvbuf, DEFAULT_BUFLEN, 0);
						receivedBuff = cleanReceivedBuffer(recvbuf);
						clientInput = std::stringstream(receivedBuff);
						break;
					}
					break;
				case Game::TurnStates::PutPieces:
					clientInput>> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(Game::TurnStates::Choose);
						send(playerIterator->second, "\nWrong number of pieces!!!\n`", DEFAULT_BUFLEN, 0);
						break;
					}
					for (int index = 0; index < playerChoice; index++)
					{
						clientInput >> pos;
						clientInput >> tempIndex;
						if (tempIndex < 1 || tempIndex>7 || pos.GetPosition() < 0 || pos.GetPosition() > Board::NumberOfTiles)
						{
							break;
						}
						tempIndex--;
						checkPiecePos.insert(tempIndex);
						checkBoardPos.insert(pos.GetPosition());
					}


					if (checkBoardPos.size() == checkPiecePos.size() && checkPiecePos.size() == playerChoice)
					{
						auto boardPosIt = checkBoardPos.begin();
						auto piecePosIt = checkPiecePos.begin();
						while (boardPosIt != checkBoardPos.end() && piecePosIt != checkPiecePos.end())
						{
							pieceIndexesInMove.push_back(*piecePosIt);
							playerMove.emplace_back(playerIterator->first.seePiece(*piecePosIt), *boardPosIt);
							boardPosIt++;
							piecePosIt++;
						}
					}
					else
					{
						send(playerIterator->second, "\nInvalid move!!!\n`", DEFAULT_BUFLEN, 0);
						gameLogger.log("A player made wrong move ....", Logger::Level::Warning);
						turnStates.push(Game::TurnStates::Choose);
						recv(playerIterator->second, recvbuf, DEFAULT_BUFLEN, 0);
						receivedBuff = cleanReceivedBuffer(recvbuf);
						clientInput = std::stringstream(receivedBuff);
						break;
					}


					if (validatePlayerMove(playerMove) == true)
					{
						moveWay = validatePositioning(playerMove);
						std::sort(pieceIndexesInMove.rbegin(), pieceIndexesInMove.rend());
						std::sort(playerMove.begin(), playerMove.end(), compare);
						for (auto pos : pieceIndexesInMove)
						{
							playerIterator->first.putPiece(pos);
						}
						makeMove(playerMove);
						switch (moveWay)
						{
						case Game::MoveType::Vertical:
							newWordEnd = (playerMove.end() - 1)->second;
							newWordStart = playerMove.begin()->second;
							findNewWordBounds(newWordStart, newWordEnd, Board::BoardSize);
							playerIterator->first.incrementScore(valueOfNewWord(newWordStart, newWordEnd, Board::BoardSize));
							break;
						case Game::MoveType::Orizontal:
							newWordEnd = (playerMove.end() - 1)->second;
							newWordStart = playerMove.begin()->second;
							findNewWordBounds(newWordStart, newWordEnd, 1);
							playerIterator->first.incrementScore(valueOfNewWord(newWordStart, newWordEnd, 1));
							break;
						}
						turnStates.push(Game::TurnStates::NextPlayer);
					}
					else
					{
						gameLogger.log("A player made wrong move ....", Logger::Level::Warning);
						send(playerIterator->second, "\nWrong move!!!\n`", DEFAULT_BUFLEN, 0);
						turnStates.push(Game::TurnStates::Choose);
						recv(playerIterator->second, recvbuf, DEFAULT_BUFLEN, 0);
						receivedBuff = cleanReceivedBuffer(recvbuf);
						clientInput = std::stringstream(receivedBuff);
					}
					break;
				case Game::TurnStates::ExchangePieces:
					clientInput >> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input ....", Logger::Level::Warning);
						send(playerIterator->second, "\nWrong number of pieces!!!\n`", DEFAULT_BUFLEN, 0);
						recv(playerIterator->second, recvbuf, DEFAULT_BUFLEN, 0);
						receivedBuff = cleanReceivedBuffer(recvbuf);
						clientInput = std::stringstream(receivedBuff);
						break;
					}
					for (int index = 0; index < playerChoice; index++)
					{
						clientInput >> tempIndex;
						if (tempIndex < 1 || tempIndex>7)
						{
							break;
						}
						tempIndex--;
						pieceIndexes.insert(tempIndex);
					}
					if (pieceIndexes.size() != playerChoice)
					{
						send(playerIterator->second, "\nWrong piece index!!!\n`", DEFAULT_BUFLEN, 0);
						turnStates.push(Game::TurnStates::Choose);
						gameLogger.log("A player inserted wrong input ....", Logger::Level::Warning);
						recv(playerIterator->second, recvbuf, DEFAULT_BUFLEN, 0);
						receivedBuff = cleanReceivedBuffer(recvbuf);
						clientInput = std::stringstream(receivedBuff);
					}
					else
					{
						for (auto it = pieceIndexes.rbegin(); it != pieceIndexes.rend(); it++)
						{
							m_playingBag.putPieceBackInBag(playerIterator->first.putPiece(*it));
						}
						turnStates.push(Game::TurnStates::NextPlayer);
					}
					break;
				case Game::TurnStates::NextPlayer:
					while (!m_playingBag.isEmpty() && playerIterator->first.GetNumberOfPieces() < numberOfPlayerPieces)
					{
						auto taken = m_playingBag.takeOnePiece();
						playerIterator->first.takePiece(taken);
					}
					playerIterator++;
					if (playerIterator == players.end())
					{
						playerIterator = players.begin();
					}
					break;
				}
			}
			sendBuffaux = m_playingBoard.toString();
			pieceIndexesInMove.clear();
			playerMove.clear();
		}
	
	auto winner = GetWinner(players);
	gameLogger.log("A player won\nExiting.....", Logger::Level::Info);
	std::string gameOverString = "\nGAME OVER!\nWinner is Player #" + std::to_string(winner - players.begin() + 1)+"`";
	for (auto player : players)
	{
		send(player.second, gameOverString.c_str(), DEFAULT_BUFLEN, 0);
		shutdown(player.second, SD_SEND);
	}
	closesocket(players[0].second);
	closesocket(players[1].second);
	WSACleanup();
	of.close();
}

Game::Game()
{
}

Game::MoveType Game::validatePositioning(const std::vector<std::pair<Piece, int>>& playerMove)
{
	bool vertical = true, orizontal = true;
	for (auto index = 0; index < playerMove.size() - 1; index++)
	{
		if (abs(playerMove[index].second - playerMove[index + 1].second) % Board::BoardSize != 0)
		{
			vertical = false;
			break;
		}
	}
	if (vertical)
	{
		return MoveType::Vertical;
	}
	for (auto index = 0; index < playerMove.size() - 1; index++)
	{
		if (playerMove[index].second / Board::BoardSize != playerMove[index + 1].second / Board::BoardSize)
		{
			orizontal = false;
			break;
		}

		if (orizontal)
		{
			return MoveType::Orizontal;
		}

	}
	return MoveType::Invalid;
}

bool Game::validatePlayerMove(const std::vector<std::pair<Piece, int>>& playerMove)
{
	std::string word;
	switch (validatePositioning(playerMove))
	{
	case MoveType::Invalid:
		return false;
		break;
	case MoveType::Vertical:
		word = makeWord(playerMove, Board::BoardSize);
		if (moveCanBeDone(playerMove, Board::BoardSize) && word != "invalid" && checkIfWordIsReal(word))
		{
			return true;
		}
		break;
	case MoveType::Orizontal:
		word = makeWord(playerMove, 1);
		if (moveCanBeDone(playerMove, 1) && word != "invalid"&& checkIfWordIsReal(word))
		{
			return true;
		}
		break;
	}
	return false;
}

bool Game::moveCanBeDone(const std::vector<std::pair<Piece, int>>& playerMove, int increment)
{
	if (!m_playingBoard[112].second.has_value())
	{
		return true;
	}
	auto edgePosition = playerMove.begin()->second - increment;
	if (edgePosition >= NULL && m_playingBoard[edgePosition].second.has_value())
	{
		return true;
	}
	edgePosition = (playerMove.end() - 1)->second + increment;
	if (edgePosition < Board::NumberOfTiles && m_playingBoard[edgePosition].second.has_value())
	{
		return true;
	}
	for (auto it = playerMove.begin(); it != playerMove.end() - 1; it++)
	{
		for (auto pos = it->second; pos < (it + 1)->second; pos += increment)
		{
			if (m_playingBoard[pos].second.has_value())
			{
				return true;
			}
		}
	}
	return false;
}

std::string Game::makeWord(const std::vector<std::pair<Piece, int>>& playerMove, int increment)
{
	std::string resultingWord;
	if (playerMove.begin()->second > increment)
	{
		auto index = playerMove.begin()->second - increment;
		while (index > NULL && m_playingBoard[index].second.has_value())
		{
			resultingWord += m_playingBoard[index].second->toChar();
			index -= increment;
		}


	}
	for (auto index = 0; index < playerMove.size() - 1; index++)
	{
		resultingWord += playerMove[index].first.toChar();
		for (auto pos = playerMove[index].second + increment; pos < playerMove[index + 1].second; pos += increment)
		{
			if (!m_playingBoard[pos].second.has_value())
			{
				return "invalid";
			}
			resultingWord += m_playingBoard[pos].second->toChar();
		}

	}
	if ((playerMove.end() - 1)->second + increment > Board::NumberOfTiles)
	{
		auto index = (playerMove.end() - 1)->second + increment;
		while (index < Board::NumberOfTiles && m_playingBoard[index].second.has_value())
		{
			resultingWord += m_playingBoard[index].second->toChar();
			index += increment;
		}


	}
	return resultingWord;
}

bool Game::checkIfWordIsReal(std::string word)
{
	auto findResult = word.find(" ");
	if (findResult != std::string::npos)
	{
		std::cout << "\nWhich letter should the empty piece be?\n";
		char letter;
		std::cin >> letter;
		if (!m_dictionary.isAWord(word.replace(findResult, 1, &letter)))
		{
			return false;
		}
	}
	return true;
}

void Game::makeMove(const std::vector<std::pair<Piece, int>>& playerMove)
{
	for (const auto &[piece, position] : playerMove)
	{
		m_playingBoard.putPiece(piece, position);
	}
}

int Game::valueOfNewWord(int firstLetter, int lastLetter, int increment)
{
	int wordMultiplier = 1, wordScore = 0;

	for (auto index = firstLetter; index <= lastLetter; index += increment)
	{
		switch (m_playingBoard[index].first)
		{
		case Board::TileType::DL:
			wordScore += 2 * m_playingBoard[index].second->getValue();
			break;
		case Board::TileType::TL:
			wordScore += 3 * m_playingBoard[index].second->getValue();
			break;
		case Board::TileType::DW:
		case Board::TileType::ST:
			wordScore += m_playingBoard[index].second->getValue();
			wordMultiplier *= 2;
			break;
		case Board::TileType::TW:
			wordScore += m_playingBoard[index].second->getValue();
			wordMultiplier *= 3;
			break;
		case Board::TileType::None:
			wordScore += m_playingBoard[index].second->getValue();
			break;
		}
	}
	return wordScore * wordMultiplier;
}

void Game::findNewWordBounds(int & firstLetter, int & lastLetter, int increment)
{
	while (firstLetter >= 0 && m_playingBoard[firstLetter].second.has_value())
	{
		firstLetter -= increment;
	}
	while (lastLetter < Board::NumberOfTiles && m_playingBoard[lastLetter].second.has_value())
	{
		lastLetter += increment;
	}
}



std::vector<Player>::iterator Game::GetWinner(std::vector<Player>players)
{
	std::vector<Player>::iterator it;
	int biggestScore = 0;
	for (auto player = players.begin(); player < players.end(); player++)
	{
		if (player->GetScore() > biggestScore)
		{
			biggestScore = player->GetScore();
			it = player;
		}
	}
	return it;
}

std::vector<std::pair<Player, SOCKET>>::iterator Game::GetWinner(std::vector<std::pair<Player, SOCKET>>players)
{
	std::vector<std::pair<Player, SOCKET>>::iterator it;
	int biggestScore = 0;
	for (auto player = players.begin(); player < players.end(); player++)
	{
		if (player->first.GetScore() > biggestScore)
		{
			biggestScore = player->first.GetScore();
			it = player;
		}
	}
	return it;
}

std::string Game::cleanReceivedBuffer(const char * buffer)
{
	std::string newBuffer = buffer;
	while (newBuffer.back()!= '`')
	{
		newBuffer.pop_back();
	}
	newBuffer.pop_back();
	return newBuffer;
}



