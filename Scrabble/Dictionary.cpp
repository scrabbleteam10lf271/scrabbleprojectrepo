#include "Dictionary.h"

bool Dictionary::isAWord(std::string word) const
{
	return dictionary.find(word) != dictionary.end();
}

Dictionary::Dictionary(std::ifstream inputFile)
{
	std::string word;
	while (!inputFile.eof())
	{
		inputFile >> word;
		this->dictionary.insert(word);
	}
}

Dictionary::~Dictionary()
{
	this->dictionary.clear();
}