#include "Bag.h"

#include<random>
#include<chrono>

const Piece & Bag::takeOnePiece()
{
	std::default_random_engine generator;
	std::uniform_int_distribution<unsigned int> distribution(0, bagOfPieces.size() - 1);
	unsigned int index = distribution(generator);
	Piece removedPiece = bagOfPieces[index];
	bagOfPieces.erase(bagOfPieces.begin() + index);
	return removedPiece;
}

void Bag::putPieceBackInBag(const Piece& piece)
{
	bagOfPieces.push_back(piece);
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(bagOfPieces.begin(), bagOfPieces.end(), std::default_random_engine(seed));
}

bool Bag::isEmpty() const
{
	return bagOfPieces.empty();
}

Bag::Bag()
{
	bagOfPieces.insert(bagOfPieces.end(), 9, Piece(Piece::Letter::A));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::B));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::C));
	bagOfPieces.insert(bagOfPieces.end(), 4, Piece(Piece::Letter::D));
	bagOfPieces.insert(bagOfPieces.end(), 12, Piece(Piece::Letter::E));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::F));
	bagOfPieces.insert(bagOfPieces.end(), 3, Piece(Piece::Letter::G));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::H));
	bagOfPieces.insert(bagOfPieces.end(), 9, Piece(Piece::Letter::I));
	bagOfPieces.insert(bagOfPieces.end(), 1, Piece(Piece::Letter::J));
	bagOfPieces.insert(bagOfPieces.end(), 1, Piece(Piece::Letter::K));
	bagOfPieces.insert(bagOfPieces.end(), 4, Piece(Piece::Letter::L));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::M));
	bagOfPieces.insert(bagOfPieces.end(), 6, Piece(Piece::Letter::N));
	bagOfPieces.insert(bagOfPieces.end(), 8, Piece(Piece::Letter::O));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::P));
	bagOfPieces.insert(bagOfPieces.end(), 1, Piece(Piece::Letter::Q));
	bagOfPieces.insert(bagOfPieces.end(), 6, Piece(Piece::Letter::R));
	bagOfPieces.insert(bagOfPieces.end(), 4, Piece(Piece::Letter::S));
	bagOfPieces.insert(bagOfPieces.end(), 6, Piece(Piece::Letter::T));
	bagOfPieces.insert(bagOfPieces.end(), 4, Piece(Piece::Letter::U));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::V));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::W));
	bagOfPieces.insert(bagOfPieces.end(), 1, Piece(Piece::Letter::X));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::Y));
	bagOfPieces.insert(bagOfPieces.end(), 1, Piece(Piece::Letter::Z));
	bagOfPieces.insert(bagOfPieces.end(), 2, Piece(Piece::Letter::None));
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(bagOfPieces.begin(), bagOfPieces.end(), std::default_random_engine(seed));
}

Bag::~Bag()
{
}