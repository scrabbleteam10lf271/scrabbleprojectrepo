#include "Board.h"

#include<iostream>
#include<string>


Board::Board()
{
	m_board[NULL] = m_board[BoardSize - 1] = m_board[(BoardSize - 1)*BoardSize] = m_board[NumberOfTiles - 1] = std::make_pair(Board::TileType::TW,std::optional<Piece>());
	m_board[3] = m_board[11] = m_board[BoardSize * 3] = m_board[BoardSize * 11] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[7] = m_board[BoardSize * 7] = m_board[(BoardSize * 8) - 1] = m_board[BoardSize *(BoardSize - 1) + 7];
	m_board[BoardSize + 1] = m_board[BoardSize * 2 - 2] = m_board[BoardSize * 13 + 1] = m_board[(BoardSize * 14) - 2] = std::make_pair(Board::TileType::DW, std::optional<Piece>());
	m_board[BoardSize + 5] = m_board[BoardSize * 2 - 6] = m_board[BoardSize * 13 + 5] = m_board[(BoardSize * 14) - 6] = std::make_pair(Board::TileType::TL,std::optional<Piece>());
	m_board[BoardSize * 2 +2] = m_board[BoardSize * 3 - 3] = m_board[BoardSize * 12 + 2] = m_board[(BoardSize * 13) - 3] = std::make_pair(Board::TileType::DW, std::optional<Piece>());
	m_board[BoardSize * 2 + 6] = m_board[BoardSize * 3 - 7] = m_board[BoardSize * 12 + 6] = m_board[(BoardSize * 13) - 7] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[BoardSize * 2 + 2] = m_board[BoardSize * 3 - 3] = m_board[BoardSize * 12 + 2] = m_board[(BoardSize * 13) - 3] = std::make_pair(Board::TileType::DW, std::optional<Piece>());
	m_board[BoardSize * 3] = m_board[BoardSize * 11] = m_board[(BoardSize * 4) - 1] = m_board[(BoardSize * 12) - 1] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[(BoardSize * 3) + 3] = m_board[(BoardSize * 11) + 3] = m_board[(BoardSize * 4) - 4] = m_board[(BoardSize * 12) - 4] = std::make_pair(Board::TileType::DW, std::optional<Piece>());
	m_board[(BoardSize * 3) + 7] = m_board[(BoardSize * 7) + 3] = m_board[(BoardSize * 8) - 4] = m_board[(BoardSize * 11) + 7] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[(BoardSize * 4) + 5] = m_board[(BoardSize * 5) - 6] = m_board[(BoardSize * 10) + 5] = m_board[(BoardSize * 11) -6] = std::make_pair(Board::TileType::DW, std::optional<Piece>());
	m_board[(BoardSize * 5) + 1] = m_board[(BoardSize * 6) -2] = m_board[(BoardSize * 9) + 1] = m_board[(BoardSize * 10) - 2] = std::make_pair(Board::TileType::TL, std::optional<Piece>());
	m_board[(BoardSize * 5) + 5] = m_board[(BoardSize * 6) - 6] = m_board[(BoardSize * 9) +5] = m_board[(BoardSize * 10) -6] = std::make_pair(Board::TileType::TL, std::optional<Piece>());
	m_board[(BoardSize * 6) + 2] = m_board[(BoardSize * 7) - 3] = m_board[(BoardSize * 8) + 2] = m_board[(BoardSize * 9) -3] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[(BoardSize * 6) + 6] = m_board[(BoardSize * 7) - 7] = m_board[(BoardSize * 8) +6 ] = m_board[(BoardSize * 9) - 7] = std::make_pair(Board::TileType::DL, std::optional<Piece>());
	m_board[(BoardSize * 7) + 7] = std::make_pair(Board::TileType::ST, std::optional<Piece>());
	
}


Board::~Board()
{
}

bool Board::putPiece(const Piece & piece, const int position)
{
	if (m_board[position].second.has_value())
	{
		return false;
	}
	else
	{
		m_board[position].second.emplace(piece);
		return true;
	}
}

std::string Board::GameLegend()
{
	return "\nLegend:\nDL : Double letter score!\nTL : Tripple letter score!\nDW : Double word score!\nTW : Triple word score!\n* : Start position!\n";
}

std::string Board::toString()const
{
	std::string string;
	string+="    ";
	for (int index = 1; index <=BoardSize; index++)
	{
		if (index < 10)
		{

			string += "  " +std::to_string(index) +"  ";
		}
		else
		{
			string += ' ' + std::to_string(index) + "  ";
		}
	}
	string += "\n    ";
	for (int index = 0; index < BoardSize; index++)
	{
		string += "_____";
	}
	string += '\n';
	for (int firstIndex = 0; firstIndex < BoardSize; firstIndex++) {
		if (firstIndex + 1 < 10)
		{
			string += std::to_string(firstIndex+ 1 ) + "  |";
		}
		else
		{
			string += std::to_string(firstIndex + 1) + " |";
		}
		for (int secondIndex = 0; secondIndex < BoardSize; secondIndex++)
		{
			int position = firstIndex * BoardSize + secondIndex;
			if (m_board[position].second.has_value())
			{
				string+="  " +std::to_string(m_board[position].second->toChar())+ "  ";
			}
			else
			{
				switch (m_board[position].first)
				{
				case Board::TileType::None:
					string += "     ";
					break;
				case Board::TileType::DL:
					string += " D L ";
					break;
				case Board::TileType::DW:
					string += " D W ";
					break;
				case Board::TileType::TL:
					string += " T L ";
					break;
				case Board::TileType::TW:
					string += " T W ";
					break;
				case Board::TileType::ST:
					string += "  *  ";
					break;
				}
			}
		}
		string += '|';
		string += "\n";
		string += "   |";
		for (int secondIndex = 0; secondIndex < BoardSize; secondIndex++)
		{
			string += "_____";
		}
		 string += '|';
		 string += "\n";
	}
	return string;

}

const std::pair<Board::TileType, std::optional<Piece>>& Board::operator[](int position)
{
	return m_board[position];
}






std::ostream & operator<<(std::ostream & os, const Board & board)
{
	os << board.toString();
	return os;
}
