#include "BoardPosition.h"



BoardPosition::BoardPosition()
{
	m_position = 0;
}


BoardPosition::~BoardPosition()
{
}

const int BoardPosition::GetPosition() const
{
	return m_position;
}

std::pair<int, int> BoardPosition::fromIndexToRowColon()
{
	int row, colon;
	row = m_position / sizeOfTheBoard;
	colon = m_position-row * sizeOfTheBoard;
	return std::make_pair(row + 1, colon + 1);
}

const int BoardPosition::fromRowClolonToIndex(int row, int colon)
{
	return (row-1)*sizeOfTheBoard+colon-1;
}

std::ostream & operator<<(std::ostream & stream, BoardPosition & object)
{
	auto rowCol = object.fromIndexToRowColon();
	stream << "(" << rowCol.first << "," << rowCol.second << ")";
	return stream;
}

std::istream & operator>>(std::istream & stream, BoardPosition & object)
{
	int row, colon;
	stream >> row >> colon;
	object.m_position = BoardPosition::fromRowClolonToIndex(row, colon);
	return stream;

}
