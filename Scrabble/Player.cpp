#include "Player.h"



int Player::GetScore()
{
	return m_score;
}

void Player::incrementScore(int with)
{
	m_score += with;
}

void Player::setName(const std::string name)
{
	m_name = name;
}

void Player::takePiece(const Piece & piece)
{
	m_playerPieces.push_back(piece); 
}

const Piece & Player::seePiece(int index)
{
	return m_playerPieces[index];
}

Piece && Player::putPiece(int index)
{
	Piece toReturn = m_playerPieces[index];
	m_playerPieces.erase(m_playerPieces.begin() + index);
	return std::move(toReturn);
}

const std::string& Player::GetName()
{
	return m_name;
}

int Player::GetNumberOfPieces()
{
	return m_playerPieces.size();
}

const std::string & Player::getPlayerBoardString()
{
	std::string board;
	for (int index = 1; index < 8; index++)
	{
		board+=std::to_string(index) + " ";
	}
	board += "\n";
	for (Piece& piece : m_playerPieces)
	{
		board += piece.toChar() + ' ';
	}
	board += "\n";
	for (Piece& piece : m_playerPieces)
	{
		board += std::to_string(piece.getValue()) + " ";
	}
	board += "\n";
	return board;
}


Player::Player()
{
	m_score = 0;	
	m_name = "notSet";
}


Player::~Player()
{
}

std::istream & operator>>(std::istream & stream, Player & object)
{
	stream >> object.m_name;
	return stream;
}

std::ostream & operator<<(std::ostream & stream, Player & object)
{

	for (int index = 1; index < 8; index++)
	{
		stream << index << " ";
	}
	stream<< "\n";
	for (Piece& piece : object.m_playerPieces)
	{
		stream << piece.toChar()<<" ";
	}
	stream << "\n";
	for (Piece& piece : object.m_playerPieces)
	{
		stream << (int)piece.getValue() << " ";
	}
	stream << "\n";
	return stream;
}
