#pragma once
#include <array>
#include <utility>
#include <optional>
#include "Piece.h"
#include <iostream>


class Board
{
public:
	Board();
	~Board();
	bool putPiece(const Piece & piece, const int position);
	static std::string GameLegend();
	std::string toString()const;

	enum class TileType
	{
		None, DL, TL, DW, TW, ST
	};
	const std::pair<Board::TileType, std::optional<Piece>>&operator[](int position);
	static const int BoardSize = 15;
	static const int NumberOfTiles = BoardSize * BoardSize;
	
private:
	friend std::ostream & operator <<(std::ostream & os, const Board & board);
	std::array<std::pair<TileType, std::optional<Piece>>, NumberOfTiles>m_board;


};

