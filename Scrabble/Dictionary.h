#pragma once

#include<string>
#include<fstream>
#include<set>

class Dictionary
{
private:
	std::set<std::string> dictionary;
public:
	bool isAWord(std::string word)const;

	Dictionary(std::ifstream inputFile=std::ifstream("Dictionary.txt"));
	~Dictionary();
};