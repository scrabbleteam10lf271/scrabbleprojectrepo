#pragma once
#include"Piece.h"

#include<vector>

class Bag
{
private:
	std::vector<Piece> bagOfPieces;
public:
	const Piece& takeOnePiece();
	void putPieceBackInBag(const Piece& piece);
	bool isEmpty()const;
	Bag();
	~Bag();
};

