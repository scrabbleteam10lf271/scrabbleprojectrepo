#pragma once
#include <iostream>
#include <utility>
class BoardPosition
{
public:
	BoardPosition();
	~BoardPosition();
	friend std::ostream& operator <<(std::ostream& stream, BoardPosition& object);
	friend std::istream& operator >>(std::istream& stream, BoardPosition& object);
	const int GetPosition()const;
private:
	static const int sizeOfTheBoard = 15;
	std::pair<int, int> fromIndexToRowColon();
	static const int fromRowClolonToIndex(int row, int colon);
	int m_position;
};

