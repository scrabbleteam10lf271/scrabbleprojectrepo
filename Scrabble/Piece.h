#pragma once
#include <ostream>
class Piece
{
public:
	Piece();
	~Piece();
	enum class Letter
	{
		None,
		A,
		E,
		I,
		L,
		N,
		O,
		R,
		S,
		T,
		U,
		D,
		G,
		B,
		C,
		M,
		P,
		F,
		H,
		V,
		W,
		Y,
		K,
		J,
		X,
		Q,
		Z,
	};

	Piece(const Piece::Letter &letter);
	friend std::ostream & operator <<(std::ostream & os, const Piece &piece);
	int getValue() const;
	void setValue(int  letter);
	Piece::Letter getLetter()const;
	void setLetter(const Letter & value);
	char  toChar()const;
private :
	Letter m_letter;
	int m_value;

};

