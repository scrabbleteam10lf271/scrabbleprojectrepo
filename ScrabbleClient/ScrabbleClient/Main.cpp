#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <stack>
#include <sstream>
#include <string>


#pragma comment (lib, "Ws2_32.lib")
#pragma comment (lib, "Mswsock.lib")
#pragma comment (lib, "AdvApi32.lib")


#define DEFAULT_BUFLEN 10240
#define DEFAULT_PORT "27015"


enum class TurnStates
{
	Choose, PutPieces, ExchangePieces, NextPlayer
};

std::string cleanReceivedBuffer(const char * buffer)
{
	std::string newBuffer = buffer;
	while (newBuffer.back() != '`')
	{
		newBuffer.pop_back();
	}
	newBuffer.pop_back();
	return newBuffer;
}

int __cdecl main()
{
	std::string received;
	std::string toSend;
	WSADATA wsaData;
	SOCKET ConnectSocket = INVALID_SOCKET;
	struct addrinfo *result = NULL,
		*ptr = NULL,
		hints;
	char *sendbuf = (char*)malloc(DEFAULT_BUFLEN * sizeof(char));
	char recvbuf[DEFAULT_BUFLEN];
	int iResult;
	int recvbuflen = DEFAULT_BUFLEN;
	int playerChoice;
	int xPosition, yPosition, pieceIndex;
	std::stack<TurnStates>turnStates;
	TurnStates currentState;



	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		printf("WSAStartup failed with error: %d\n", iResult);
		return 1;
	}

	ZeroMemory(&hints, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	iResult = getaddrinfo(NULL, DEFAULT_PORT, &hints, &result);
	if (iResult != 0) {
		printf("getaddrinfo failed with error: %d\n", iResult);
		WSACleanup();
		return 1;
	}

	for (ptr = result; ptr != NULL; ptr = ptr->ai_next) {
		ConnectSocket = socket(ptr->ai_family, ptr->ai_socktype,
			ptr->ai_protocol);
		if (ConnectSocket == INVALID_SOCKET) {
			printf("socket failed with error: %ld\n", WSAGetLastError());
			WSACleanup();
			return 1;
		}

		iResult = connect(ConnectSocket, ptr->ai_addr, (int)ptr->ai_addrlen);
		if (iResult == SOCKET_ERROR) {
			closesocket(ConnectSocket);
			ConnectSocket = INVALID_SOCKET;
			continue;
		}
		break;
	}

	freeaddrinfo(result);

	if (ConnectSocket == INVALID_SOCKET) {
		printf("Unable to connect to server!\n");
		WSACleanup();
		return 1;
	}
	iResult = 1;
	while (true)
	{
		recv(ConnectSocket, recvbuf, recvbuflen, 0);
		received = cleanReceivedBuffer(recvbuf);
		if (received.find("wait")!=std::string::npos)
		{	
			std::cout << received.substr(NULL, received.find("wait"));
			continue;
		}
		else if (received.find("GAME OVER") != std::string::npos)
		{
			std::cout << received << " ";
			system("Pause");
			break;
		}
		else
		{
			std::cout << received << '\n';
			turnStates.push(TurnStates::Choose);
			while (!turnStates.empty())
			{
				currentState = turnStates.top();
				turnStates.pop();
				switch (currentState)
				{
				case TurnStates::Choose:
					std::cout << "\nYou can:\n1)Put pieces on the table\n2)Exchange pieces with new ones\n3)Pass\nWhat will you do: ";
					std::cin >> playerChoice;
					switch (playerChoice)
					{
					case 1:
						turnStates.push(TurnStates::PutPieces);
						toSend += "1 ";
						break;
					case 2:
						turnStates.push(TurnStates::ExchangePieces);
						toSend += "2 ";
						break;
					case 3:
						turnStates.push(TurnStates::NextPlayer);
						toSend += "3 ";
						break;
					default:
						turnStates.push(TurnStates::Choose);
						std::cout << "\nWrong choice!!!";
						break;
					}
					break;
				case TurnStates::PutPieces:
					std::cout << "\nHow many pieces do you want to put on the table ?(1-7) :";
					std::cin >> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(TurnStates::Choose);
						toSend = "";
						std::cout << "\nWrong number of pieces!!!";
						break;
					}
					toSend += std::to_string(playerChoice) + " ";
					for (int index = 0; index < playerChoice; index++)
					{
						std::cout << "\nInsert where you want to put the piece (x,y) : ";
						std::cin >> xPosition >> yPosition;
						std::cout << "\nInsert piece index(1-7) : ";
						std::cin >> pieceIndex;
						if (pieceIndex < 1 || pieceIndex>7 || xPosition < 1 || xPosition > 15 || yPosition < 1 || yPosition > 15)
						{
							turnStates.push(TurnStates::Choose);
							toSend = "";
							break;
						}
						toSend += std::to_string(pieceIndex) + " ";
						toSend += std::to_string(xPosition) + " ";
						toSend += std::to_string(yPosition) + " ";
					}
					break;
				case TurnStates::ExchangePieces:
					std::cout << "\nHow many pieces do you want to exchange ?(1-7) : ";
					std::cin >> playerChoice;
					if (playerChoice > 7 || playerChoice < 1)
					{
						turnStates.push(TurnStates::Choose);
						toSend = "";
						std::cout << "\nWrong number of pieces!!!";
						break;
					}
					toSend += std::to_string(playerChoice) + " ";
					for (int index = 0; index < playerChoice; index++)
					{
						std::cout << "\nInsert piece index(1-7) : ";
						std::cin >> pieceIndex;
						if (pieceIndex < 1 || pieceIndex>7)
						{
							turnStates.push(TurnStates::Choose);
							toSend = "";
							break;
						}
						toSend += std::to_string(pieceIndex) + " ";
					}
				case TurnStates::NextPlayer:
					break;
				}
			}
			toSend += "`";
			iResult = send(ConnectSocket, toSend.c_str(),DEFAULT_BUFLEN, 0);
		}
	}
	iResult = shutdown(ConnectSocket, SD_SEND);
	closesocket(ConnectSocket);
	WSACleanup();

	return 0;
}